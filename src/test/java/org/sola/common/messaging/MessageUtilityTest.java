/**
 * ******************************************************************************************
 * Copyright (C) 2014 - Food and Agriculture Organization of the United Nations (FAO).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,this list
 *       of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright notice,this list
 *       of conditions and the following disclaimer in the documentation and/or other
 *       materials provided with the distribution.
 *    3. Neither the name of FAO nor the names of its contributors may be used to endorse or
 *       promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,STRICT LIABILITY,OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * *********************************************************************************************
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sola.common.messaging;

import java.lang.reflect.Field;
import java.util.Arrays;
import org.junit.Ignore;
import javax.swing.JOptionPane;
import java.util.Locale;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soladev
 */
public class MessageUtilityTest {
    private static final String CONTACT_ADMIN = "Contact your system administrator for assistance.";
    private static final String ERROR = "Error";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String NO_ACTION = "No action required.";
    private static final String MESSAGE = "Message";
    private static final String WARNING_MESSAGE = "Warning test message.";
    private static final String SOME_OTHER = "some other";
    private static final String QUESTION = "Question";
    private static final String MSG_PREFIX = "MSG_PREFIX";
    private static final String NO_FIELD = "Field could not be accessed: ";
    private static final String NO_MESSAGE = "No message for: ";
    private static final String LONG_PARAMETER = "one parameter test message 20,000 and A very very long paramter that will need to be broken at the ";
    private static final String APPROPRIATE = "appropriate";
    private static final String ERR_001 = "ERR_001";
    

    @BeforeClass
    public static void setUpClass() {
        Locale loc = new Locale("en", "US");
        Locale.setDefault(loc);
    }
    
   

    /**
     * Checks that a default message is returned for a null code. This test will fail if the
     * default locale is not en_US
     */
    @Test
    public void testGetLocalizedMessage_NullCode() {
        String msgCode=null;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        test(result, msgCode, "Unable to obtain localized message for null from locale en_US.", 
                CONTACT_ADMIN, ERROR, LocalizedMessage.Type.ERROR,
                new String[]{"OK"}, ERROR, JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        
        assertEquals("Unable to obtain localized message for null from locale en_US."
                + LINE_SEPARATOR + CONTACT_ADMIN,
                result.formatMessage(null));
        
    }

    /**
     * Checks that a default message is returned for an invalid code. This test will fail if the
     * default locale is not en_US.
     */
    @Test
    public void testGetLocalizedMessage_InvalidCode() {
        String msgCode = "invalid";
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        test(result, msgCode, "Unable to obtain localized message for invalid from locale en_US.",
              CONTACT_ADMIN, ERROR, LocalizedMessage.Type.ERROR,
              new String[]{"OK"}, "Error INVALID", JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("Unable to obtain localized message for invalid from locale en_US."
                + LINE_SEPARATOR + CONTACT_ADMIN,
                result.formatMessage("err_num"));
    }

    
    /**
     * Checks a valid message can be retrieved from the Service package
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestMsg() {
        String msgCode = ServiceMessage.TEST001;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "This is a service test message.",
                NO_ACTION, MESSAGE, LocalizedMessage.Type.PLAIN,
                new String[]{"OK"}, MESSAGE+" " + ServiceMessage.TEST001.toUpperCase(),
                JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("This is a service test message."
                + LINE_SEPARATOR + NO_ACTION,
                result.formatMessage(null));
    }

    /**
     * Checks a valid message can be retrieved from the Client package
     */
    @Test
    public void testGetLocalizedMessage_ClientTestMsg() {
        String msgCode = ClientMessage.TEST001;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "This is a client test message.",
                null, MESSAGE, LocalizedMessage.Type.PLAIN,
                new String[]{"OK"}, MESSAGE+" " + ClientMessage.TEST001.toUpperCase(),
                JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("This is a client test message.", result.formatMessage("err num"));
    }

    /**
     * Checks a valid message can be retrieved from the GIS package
     */
    @Test
    public void testGetLocalizedMessage_GISTestMsg() {
        String msgCode = GisMessage.TEST001;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "This is a gis test message.", null, MESSAGE,
                LocalizedMessage.Type.PLAIN, new String[]{"OK"}, MESSAGE+" " + GisMessage.TEST001.toUpperCase(),
                JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("This is a gis test message.", result.formatMessage(null));
    }

    /**
     * Checks that the ERROR type is correctly mapped
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestERROR() {
        String msgCode = ServiceMessage.TEST003;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "Error test message. Error Number:", null,
            ERROR, LocalizedMessage.Type.ERROR, new String[]{"OK"}, 
            "Error " + ServiceMessage.TEST003.toUpperCase(), JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("Error test message. Error Number: errnum1", result.formatMessage("errnum1"));
    }
    
    /**
     * Checks that the WARNING type is correctly mapped
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestWARNING() {
        String msgCode = ServiceMessage.TEST004;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, WARNING_MESSAGE, null, "Warning", LocalizedMessage.Type.WARNING,
                new String[]{"OK"}, "Warning " + ServiceMessage.TEST004.toUpperCase(), 
                JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals(WARNING_MESSAGE, result.formatMessage(null));
    }

    /**
     * Checks that the INFORMATION type is correctly mapped
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestINFO() {
        String msgCode = ServiceMessage.TEST005;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "Information test message.", null, "Information", 
        LocalizedMessage.Type.INFORMATION, new String[]{"OK"},
        "Information " + ServiceMessage.TEST005.toUpperCase(), 
        JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
        
        assertEquals("Information test message.", result.formatMessage(null));
    }

    /**
     * Checks that a message can have new parameters added
     */
    @Test
    public void testGetLocalizedMessage_ServiceParamTestMsg1() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", "20000", SOME_OTHER};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, "one parameter test message 20000 and some other.", null,
        QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, new String[]{"Yes", "No"}, 
        QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
        JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        
        assertEquals("one parameter test message 20000 and some other.", result.formatMessage(null));
    }

    /**
     * Checks that not all parameters need to be substituted
     */
    @Test
    public void testGetLocalizedMessage_ServiceParamTestMsg2() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", 20000};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, "one parameter test message 20,000 and {2}.", null,
                QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, 
                new String[]{"Yes", "No"}, QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        
        assertEquals("one parameter test message 20,000 and {2}.", result.formatMessage(null));
    }

    /**
     * Checks that the QUESTION_YES_NO_CANCEL type is correctly mapped
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestQYNC() {
        String msgCode = ServiceMessage.TEST006;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        
        test(result, msgCode, "Question YNC message.", null,
                QUESTION, LocalizedMessage.Type.QUESTION_YES_NO_CANCEL, 
                new String[]{"Yes", "No", "Cancel"}, 
                QUESTION+" " + ServiceMessage.TEST006.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
        
        assertEquals("Question YNC message.", result.formatMessage(null));
    }

    /**
     * Checks that the QUESTION_OK_CANCEL type is correctly mapped
     */
    @Test
    public void testGetLocalizedMessage_ServiceTestQOC() {
        String msgCode = ServiceMessage.TEST007;
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode);
        assertNotNull(result);
        assertEquals(msgCode, result.getMessageCode());
        assertEquals("Question OC message.", result.getMessage());
        assertNull(result.getAction());
        assertEquals(QUESTION, result.getTypeDescription());
        assertEquals(LocalizedMessage.Type.QUESTION_OK_CANCEL, result.getType());
        assertArrayEquals(new String[]{"Opt1", "Opt2", "Cancel"}, result.getDialogOptions());
        assertEquals("Question OC message.", result.formatMessage(null));
        assertEquals(QUESTION+" " + ServiceMessage.TEST007.toUpperCase(), result.formatTitle());
        assertEquals(JOptionPane.QUESTION_MESSAGE, MessageUtility.getJOptionPaneMessageType(result));
        assertEquals(JOptionPane.OK_CANCEL_OPTION, MessageUtility.getJOptionPaneOptionType(result));
    }

    /**
     * Checks that the test messages can be correctly displayed. Ignored as this method displays
     * a dialog window. Should be used to verify changes to the displayMessage only. 
     */
    @Test
    @Ignore
    public void testDisplayMessage_ShowMessages() {
        MessageUtility.displayMessage(ServiceMessage.TEST001, "Test1");
        Object[] parms = {"one", 20000, SOME_OTHER};
        MessageUtility.displayMessage(ServiceMessage.TEST002, "Test2", parms);
        MessageUtility.displayMessage(ServiceMessage.TEST003, "Test3");
        MessageUtility.displayMessage(ServiceMessage.TEST004, (String) null);
        MessageUtility.displayMessage(ServiceMessage.TEST005, "Test5");
        MessageUtility.displayMessage(ServiceMessage.TEST006, "Test6");
        MessageUtility.displayMessage(ServiceMessage.TEST007, "Test7");
    }

    /**
     * Checks that all message constants on the ServiceMessage class have a message in the
     * default Bundle. 
     */
    @Test
    public void checkServiceMessagesExist() {
        String out = "";
        for (Field field : ServiceMessage.class.getFields()) {
            String msgCode = null;
            if (!field.getName().equalsIgnoreCase(MSG_PREFIX)) {
                try {
                    msgCode = field.get(null).toString();
                } catch (IllegalAccessException ex) {
                    out = out + LINE_SEPARATOR
                            + NO_FIELD + field.getName();
                    break;
                }
                if (!MessageUtility.hasLocalizedMessage(msgCode)) {
                    out = out + LINE_SEPARATOR
                            + NO_MESSAGE + msgCode;
                }
            }
        }
        assertEquals("", out);
    }

    /**
     * Checks that all message constants on the ClientMessage class have a message in the
     * default Bundle. 
     */
    @Test
    public void checkClientMessagesExist() {
        String out = "";
        for (Field field : ClientMessage.class.getFields()) {
            String msgCode = null;
            if (!field.getName().equalsIgnoreCase(MSG_PREFIX)) {
                try {
                    msgCode = field.get(null).toString();
                } catch (IllegalAccessException ex) {
                    out = out + LINE_SEPARATOR
                            + NO_FIELD + field.getName();
                    break;
                }
                if (!MessageUtility.hasLocalizedMessage(msgCode)) {
                    out = out + LINE_SEPARATOR
                            + NO_MESSAGE + msgCode;
                }
            }
        }
        assertEquals("", out);
    }

    /**
     * Checks that all message constants on the GisMessage class have a message in the
     * default Bundle. 
     */
    @Test
    public void checkGISMessagesExist() {
        String out = "";
        for (Field field : GisMessage.class.getFields()) {
            String msgCode = null;
            if (!field.getName().equalsIgnoreCase(MSG_PREFIX)) {
                try {
                    msgCode = field.get(null).toString();
                } catch (IllegalAccessException ex) {
                    out = out + LINE_SEPARATOR
                            + NO_FIELD + field.getName();
                    break;
                }
                if (!MessageUtility.hasLocalizedMessage(msgCode)) {
                    out = out + LINE_SEPARATOR
                            + NO_MESSAGE + msgCode;
                }
            }
        }
        assertEquals("", out);
    }

    /**
     * Checks that long parameters are dealt with correctly
     */
    @Test
    public void testGetLocalizedMessage_LongParamTestMsg1() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", 20000, "A very very long paramter that will need to be broken at the "
            + "appropriate position otherwise it may cause some issues with viewing the message"};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, LONG_PARAMETER + APPROPRIATE + LINE_SEPARATOR
                + "position otherwise it may cause some issues with viewing the message.", null,
                QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, new String[]{"Yes", "No"}, 
                QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    }
    
    /**
     * Checks that long parameters are dealt with correctly
     */
    @Test
    public void testGetLocalizedMessage_LongParamTestMsg2() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", 20000, "A very very long paramter that will" + LINE_SEPARATOR
            + "need to be broken at the appropriate position otherwise it may cause" + LINE_SEPARATOR
            + "some issues with viewing the message"};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, LONG_PARAMETER + APPROPRIATE + LINE_SEPARATOR
                + "position otherwise it may cause some issues with viewing the message.", 
                null, QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, 
                new String[]{"Yes", "No"}, 
                QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    }

    /**
     * Checks that long parameters are dealt with correctly
     */
    @Test
    public void testGetLocalizedMessage_LongParamTestMsg3() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", 20000, "A very very long paramter that will" + LINE_SEPARATOR
            + "need to be broken at the appropriate position otherwise it may cause" + LINE_SEPARATOR
            + "some issues with viewing the message. This has some additional text to make sure it "
            + "works as expected otherwise for things longer than 160 chars"};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, LONG_PARAMETER
                + APPROPRIATE + LINE_SEPARATOR
                + "position otherwise it may cause some issues with viewing the message. This has"
                + LINE_SEPARATOR
                + "some additional text to make sure it works as expected otherwise for things"
                + LINE_SEPARATOR
                + "longer than 160 chars.", 
                null, QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, 
                new String[]{"Yes", "No"}, 
                QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    }

    @Test
    public void testGetLocalizedMessage_LongParamTestMsg4() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", 20000, "Averyverylongparamterthatwill"
            + "needtobebrokenattheappropriatepositionotherwiseitmaycause"
            + "someissueswithviewingthemessage"};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, "one parameter test message 20,000 and Averyverylongparamterthatwillneedtobebrokenatthe"
                + "appropriatepositionotherwiseitma" + LINE_SEPARATOR
                + "ycausesomeissueswithviewingthemessage.", 
                null, QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, 
                new String[]{"Yes", "No"}, 
                QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    }
    
    /** Tests the various displayMessage apis with a null messageResponder */
     @Test
    public void testDisplayMessage_SuppressedNoResponder() {
        String msgCode = ServiceMessage.TEST001;
        MessageUtility.suppressDialog(null);
        
        int result = MessageUtility.displayMessage(msgCode);
        assertEquals(0, result); 
        
        result = MessageUtility.displayMessage(msgCode, Arrays.asList("one", "2")); 
        assertEquals(0, result); 
        
        result = MessageUtility.displayMessage(msgCode, new Object[]{"one", "2"}); 
        assertEquals(0, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001); 
        assertEquals(0, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001, Arrays.asList("one", "2")); 
        assertEquals(0, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001, new Object[]{"one", "2"}); 
        assertEquals(0, result);

    }
     
     /** Tests the various displayMessage apis with a messageResponder */
     @Test
    public void testDisplayMessage_SuppressedWithResponder() {
        String msgCode = ServiceMessage.TEST001;
        MessageUtility.suppressDialog(
                (LocalizedMessage msg, String errorNum, int defaultButton) ->
            {
                return 5; 
            });
        
        int result = MessageUtility.displayMessage(msgCode);
        assertEquals(5, result); 
        
        result = MessageUtility.displayMessage(msgCode, Arrays.asList("one", "2")); 
        assertEquals(5, result); 
        
        result = MessageUtility.displayMessage(msgCode, new Object[]{"one", "2"}); 
        assertEquals(5, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001); 
        assertEquals(5, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001, Arrays.asList("one", "2")); 
        assertEquals(5, result);
        
        result = MessageUtility.displayMessage(msgCode, ERR_001, new Object[]{"one", "2"}); 
        assertEquals(5, result);

    }
    
    @Test
    public void testGetLocalizedMessage_ServiceNullParamMsg1() {
        String msgCode = ServiceMessage.TEST002;
        Object[] parms = {"one", null, SOME_OTHER};
        LocalizedMessage result = MessageUtility.getLocalizedMessage(msgCode, parms);
        
        test(result, msgCode, "one parameter test message null and some other.", null,
        QUESTION, LocalizedMessage.Type.QUESTION_YES_NO, new String[]{"Yes", "No"}, 
        QUESTION+" " + ServiceMessage.TEST002.toUpperCase(), 
        JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        
        assertEquals("one parameter test message null and some other.", result.formatMessage(null));
    }
    
    private void test(LocalizedMessage result, String msgCode, String resultMessage, 
            String action, String typeDesc, LocalizedMessage.Type msgType, 
            String[] dialogOptions, String title, 
            int paneMessageType, int paneOptionType) {
        assertNotNull(result);
        assertEquals(msgCode, result.getMessageCode());
        assertEquals(resultMessage, result.getMessage());
        assertEquals(action, result.getAction());
        assertEquals(typeDesc, result.getTypeDescription());
        assertEquals(msgType, result.getType());
        assertArrayEquals(dialogOptions, result.getDialogOptions());
        assertEquals(title, result.formatTitle());
        assertEquals(paneMessageType, MessageUtility.getJOptionPaneMessageType(result));
        assertEquals(paneOptionType, MessageUtility.getJOptionPaneOptionType(result));
    }

}
